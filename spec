011_ (:novel has-view html_page)
012_ (:novel page_url "url")
013_ (:novel has-chapter :chapter1)
014_ (:novel has-chapter :chapter2)
021_ (:chapter1 has-view html_page)
022_ (:chapter1 page_url "url")
023_ (:chapter1 page_content "content")
024_ (:chapter1 next_in_kind :chapter2)
031_ (:chapter2 has-view html_page)
032_ (:chapter2 page_url "url")
033_ (:chapter2 page_content "content")
034_ (:chapter2 previous_in_kind :chapter1)

nVh_ (:novel has-view html_page)
nUa_ (:novel page_url "url a")
nC1_ (:novel has-chapter :chapter1)
nC2_ (:novel has-chapter :chapter2)
1Vh_ (:chapter1 has-view html_page)
12b_ (:chapter1 page_url "url b")
1Cd_ (:chapter1 page_content "content d")
1N2_ (:chapter1 next_in_kind :chapter2)
2Vh_ (:chapter2 has-view html_page)
2Uc_ (:chapter2 page_url "url c")
2Ce_ (:chapter2 page_content "content e")
2P1_ (:chapter2 previous_in_kind :chapter1)

???? (:1A :assert :2P1_)

fixed-length string cannot encode id of concepts

reference, then, must not have meaningful ids, and every concept group must have a canonical textual representation with the references replaced by :A :B :C
